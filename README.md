# The Unshallow Project

The Unshallow is a first-person arena shooter playing homage to the classic feel of DOOM and Quake with adaptive, reactive, and badass�ery gameplay; all under a dark souls/gothic theme. The main objective to every level is clear: get to the exit while fucking everything up to gibs, picking up new armor, weapons, and abilities along the way, and using your knowledge and experiences to destroy said �fucking everything� throughout progressively difficult levels. The world is set in a medieval era of desolation and corrupted religion, with gruesome and mindless beings roaming the planes. So, queue the badass protagonist (the player). He is summoned to this world to cleanse evil and bring order, but in reality, he doesn�t take anything seriously, he just wants to blow shit up as horribly as possible.

# Team
> ##Game art and designer
> Alex (2Sirius)
>
> ##Game programming
> Carlos (Kroud)

## Programs Info
### Unreal Engine 4 (4.20.0) - Epic Games
### Photoshop

## Development Status
### Gameplay
> 0%

### IA
> 0%
